/* This is a Multi-Objective Genetic Algorithm program.
**********************************************************************
*  This program is the implementation of the RP2-NSGA-2 proposed by
**
*  Dr. Juan Carlos Leyva Lopez,
*  Dr. Jesus Jaime Solano Noriega
*  Dr. Diego Alonso Gastelum Chavira
*
*  The MOEA is based on the Original NSGA-2 Code copyrighted by
*
*  Kalyanmoy Deb
*
*
*  Original NSGA-2 code taken from
*
*  https://www.iitk.ac.in/kangal/codes.shtml
**********************************************************************


*/

# include <stdio.h>
# include <stdlib.h>
# include <math.h>
# include <unistd.h>
# include <time.h>

# include "global.h"
# include "rand.h"

int nreal;
int nbin;
int nobj;
int ncon;
int popsize;
double pcross_real;
double pcross_bin;
double pmut_real;
double pmut_bin;
double eta_c;
double eta_m;
int ngen;
int nbinmut;
int nrealmut;
int nbincross;
int nrealcross;
int *nbits;
double *min_realvar;
double *max_realvar;
double *min_binvar;
double *max_binvar;
int bitlength;
int choice;
int obj1;
int obj2;
int obj3;
int angle1;
int angle2;

/* MCDA*/
double **Sigma;
double *lambdaset;
double lmin;
double lmax;
int nalternatives;
double pmut_ind_bin;
int lambda_set_size;
int **ordered_pairs;

int main (int argc, char **argv)
{
    int i, k, r;
	FILE *fpt6, *fpt7;
	population *parent_pop;
	population *child_pop;
	population *mixed_pop;

	if (argc<5)
	{
		printf("\n Usage nsga2_locus.exe < mcda.in -1 sigma sigmaId #runs \n");

		exit(1);
	}

	// number of runs the NSGA2 will be performed
	r = (int)atof(argv[4]);

	seed = (double)atof(argv[1]);
	if(seed == -1)
	{
		srand ( time(NULL) );
		seed = (double)rand() / (double)RAND_MAX ;
	}
	if (seed<=0.0 || seed>=1.0)
	{
		printf("\n Entered seed value is wrong, seed value must be in (0,1) \n");
		exit(1);
	}

	char fileName1[100];
	char fileName2[10];
	char id_project[10];
	sprintf(id_project, "%s", argv[3]);
	strcpy(fileName1, "decode_");
	strcpy(fileName2, ".out");
	strcat(fileName1, id_project);
	strcat(fileName1, fileName2);
	fpt6 = fopen(fileName1,"w");

	strcpy(fileName1, "solutions_");
	strcpy(fileName2, ".out");
	strcat(fileName1, id_project);
	strcat(fileName1, fileName2);
	fpt7 = fopen(fileName1,"w");

	printf("\n Reading population size (a multiple of 4) : ");
	scanf("%d",&popsize);
	if (popsize<4 || (popsize%4)!= 0)
	{
		printf("\n population size read is : %d",popsize);
		printf("\n Wrong population size entered, hence exiting \n");
		exit (1);
	}
	printf("\n Reading number of generations : ");
	scanf("%d",&ngen);
	printf("%d", ngen);
	if (ngen<1)
	{
		printf("\n number of generations read is : %d",ngen);
		printf("\n Wrong nuber of generations entered, hence exiting \n");
		exit (1);
	}
	printf("\n Reading lambda min value : ");
	scanf("%lf",&lmin);
	printf("%lf", lmin);

	printf("\n Reading lambda max value : ");
	scanf("%lf",&lmax);
	printf("%lf", lmax);
	/* FIXED NUMBER OF OBJECTIVES */
	nobj = 3;
	/* FIXED NUMBER OF CONSTRAINTS */
	ncon = 0;

    /* MCDA */
	printf("\n Filling Sigma Matrix");
	fillSigmaMatrix(argv[2], ' ');
	printf(" -Done");
	printf("\n Discretizing lambda");
    discretizer();
    printf(" -Done");
    printf("\n Filling Crisps");
    fill_crisp();
    printf(" -Done");
    printf("\n Filling Distances");
    fill_dist();
    printf(" -Done");
    printf("\n Filling NN");
    fill_NN();
    printf(" -Done");
    nreal = nalternatives;// * (nalternatives + 1) / 2 - nalternatives;
    min_realvar = (double *)malloc(nreal*sizeof(double));
	max_realvar = (double *)malloc(nreal*sizeof(double));
	for (i=0; i<nreal; i++)
	{
		min_realvar[i] = 0;
		max_realvar[i] = nalternatives - 1;
	}
	printf ("\n Reading the probability of crossover of real variable (0.6-1.0) : ");
	scanf ("%lf",&pcross_real);
	printf("%lf", pcross_real);
	if (pcross_real<0.0 || pcross_real>1.0)
	{
		printf("\n Probability of crossover entered is : %e",pcross_real);
		printf("\n Entered value of probability of crossover of real variables is out of bounds, hence exiting \n");
		exit (1);
	}
	printf("\n Reading the probability of mutation of solutions: ");
	scanf("%lf",&pmut_ind_bin);
	printf("%lf", pmut_ind_bin);
	printf ("\n Reading the probablity of mutation of real variables (1/nreal) : ");
	scanf ("%lf",&pmut_real);
	printf("%lf", pmut_real);
	if (pmut_real<0.0 || pmut_real>1.0)
	{
		printf("\n Probability of mutation entered is : %e",pmut_real);
		printf("\n Entered value of probability of mutation of real variables is out of bounds, hence exiting \n");
		exit (1);
	}
    /* ENDS MCDA */
    if (nreal==0 && nbin==0)
    {
        printf("\n Number of real as well as binary variables, both are zero, hence exiting \n");
        exit(1);
    }
    choice=0;
    printf("\n Do you want to use gnuplot to display the results realtime (0 for NO) (1 for yes) : ");
    scanf("%d",&choice);
    printf("%d", choice);
    if (choice!=0 && choice!=1)
    {
        printf("\n Entered the wrong choice, hence exiting, choice entered was %d\n",choice);
        exit(1);
    }

    printf("\n Input data successfully entered, now performing initialization \n");

    bitlength = 0;
    if (nbin!=0)
    {
        for (i=0; i<nbin; i++)
        {
            bitlength += nbits[i];
        }
    }

    nbinmut = 0;
    nrealmut = 0;
    nbincross = 0;
    nrealcross = 0;
    parent_pop = (population *)malloc(sizeof(population));
    child_pop = (population *)malloc(sizeof(population));
    mixed_pop = (population *)malloc(sizeof(population));
    allocate_memory_pop (parent_pop, popsize);
    allocate_memory_pop (child_pop, popsize);
    allocate_memory_pop (mixed_pop, 2*popsize);


    for (k=0; k<r; k++)
    {
        randomize();
        printf("\n Initializing pop");
        initialize_pop (parent_pop);
        printf("\n Done, now performing first generation");

        evaluate_pop (parent_pop);
        assign_rank_and_crowding_distance (parent_pop);

        fflush(stdout);

        fflush(fpt6);

      for (i=2; i<=ngen; i++)
        {
    	  	printf("\n Run: %d - Generation: %d", k, i);
            selection (parent_pop, child_pop);
            mutation_pop3 (child_pop);
            evaluate_pop(child_pop);
            merge (parent_pop, child_pop, mixed_pop);
            fill_nondominated_sort (mixed_pop, parent_pop);

        }

        printf("\n Generations finished, now reporting solutions");

        report_decode(parent_pop,fpt6);
        report_nondominated(parent_pop,fpt7);
    }

    fflush(stdout);

    fflush(fpt6);
    fflush(fpt7);

    fclose(fpt6);
    fclose(fpt7);

    if (nreal!=0)
    {
        free (min_realvar);
        free (max_realvar);
    }
    if (nbin!=0)
    {
        free (min_binvar);
        free (max_binvar);
        free (nbits);
    }
    deallocate_memory_pop (parent_pop, popsize);
    deallocate_memory_pop (child_pop, popsize);
    deallocate_memory_pop (mixed_pop, 2*popsize);
    deallocate_sigma();
    deallocate_lambdaset();
    deallocate_crisp();
    free (parent_pop);
    free (child_pop);
    free (mixed_pop);
    printf("\n Routine successfully exited \n");
    return (0);
}
