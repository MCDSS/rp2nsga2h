/* Data initialization routines */
# include <stdio.h>
# include <stdlib.h>
# include <math.h>
# include "global.h"
# include "rand.h"
/* Function to initialize a population randomly */
void initialize_pop (population *pop)
{
	printf("\n --Entering initialize_pop");
	int i, j, K;

	// Initial Kmeans clusters
	K = 2;

	// Crisp Outranking Relation Matrix
	printf("\n Q[nalternatives][nalternatives]");
	int Q[nalternatives][nalternatives];

	// Calculates Preferences Matrix
	printf("\n --calculate_preferenceMatrix");
	calculate_preferenceMatrix(lmin, nalternatives, Q);

	// Distance matrix
	double d[nalternatives][nalternatives];

	// Generates Distance Matrix
	printf("\n --distance_matrix");
	distance_matrix(nalternatives, Q, d);

	// MST vector
	int mst[nalternatives];

	// Generates the MST
	printf("\n --generate_mst");
	generate_mst(mst, nalternatives, Q, d);

	// First individual lambda value
	pop->ind[0].obj[0] = get_min_lambda();
	pop->ind[0].S = (sigma_crisp *)get_s_crisp(pop->ind[0].obj[0] * -1);

	// Fill first individual
	printf("\n --Fill first individual");
	for (j = 0; j < nalternatives; ++j)
	{
		pop->ind[0].xreal[j] = mst[j];
	}

    printf("\n --For loop");
    for (i = 1; i < popsize; i++)
    {
    	// Lambda value
    	pop->ind[i].obj[0] = get_min_lambda();
    	pop->ind[i].S = (sigma_crisp *)get_s_crisp(pop->ind[i].obj[0] * -1);

    	// First half part MST solutions
    	if(i < popsize / 2)
    	{
    		double max_dist = -1;
    		int link_cut = -1;

    		for (j = 0; j < nalternatives; ++j)
    		{
    			if(d[j][mst[j]] > max_dist && !(Q[j][mst[j]] && Q[mst[j]][j]))
    			{
    				max_dist = d[j][mst[j]];
    				link_cut = j;
    			}
			}

    		if(link_cut == -1)
    		{
        		for (j = 0; j < nalternatives; ++j)
        		{
        			if(d[j][mst[j]] > max_dist)
        			{
        				max_dist = d[j][mst[j]];
        				link_cut = j;
        			}
    			}
    		}

    		if(link_cut > -1)
    		{
    			mst[link_cut] = link_cut;
    		}

    		for (j=0; j < nalternatives; j++)
			{
				pop->ind[i].xreal[j] = mst[j];
			}
    	}
    	else
    	{
    		int parent[nalternatives];

    		initialize_pop_k_means(K, parent, nalternatives, Q, d);

    		for (j=0; j < nalternatives; j++)
			{
				pop->ind[i].xreal[j] = parent[j];
			}

    		if (K < nalternatives)
    			K++;
    		else
    			K = 2;
    	}

    }
    return;
}

void initialize_pop_mutation (population *pop)
{
    int i, j, k;
    for (i=0; i<popsize; i++)
    {
    	pop->ind[i].obj[0] = get_lambda(lmin, lmax);
    	pop->ind[i].S = (sigma_crisp *)get_s_crisp(pop->ind[i].obj[0] * -1);

    	int Q[nalternatives][nalternatives];
    	double d[nalternatives][nalternatives];
    	calculate_preferenceMatrix(pop->ind[i].obj[0] * -1, nalternatives, Q);
    	distance_matrix(nalternatives, Q, d);

    	//initializePopulationDeterministic(&(pop->ind[i]));

    	for (j=0; j<nreal; j++)
		{
    		int indifferent_set_size;
    		int indifferent_set[nalternatives];

    		indifferent_set_size = 0;

    		for (k = 0; k < nalternatives; ++k)
			{
				indifferent_set[k] = 0;
			}

    		for (k = 0; k < nalternatives; ++k)
			{
				if(Q[j][k] == 1 && Q[k][j] == 1)
				{
					indifferent_set[indifferent_set_size] = k;
					indifferent_set_size++;
				}
			}

    		double rndNumber = randomperc();
			double low, high;
			low = high  = 0.0;

			int r;
			double sum;
			sum = 0;
			for (r = 0; r < indifferent_set_size; r++)
			{
				sum += 1 - d[j][indifferent_set[r]];
			}

			for (r = 0; r < indifferent_set_size; r++)
			{
				high +=  (1 - d[j][indifferent_set[r]])/sum;
				if(low <= rndNumber && rndNumber <= high)
				{
					pop->ind[i].xreal[j] = indifferent_set[r];
					break;
				}

				low = high;
			}
		}
    }
    return;
}

/* Function to initialize a population randomly */
void initialize_pop_random (population *pop)
{
    int i, j;
    for (i=0; i<popsize; i++)
    {
    	pop->ind[i].obj[0] = get_lambda(lmin, lmax);
    	pop->ind[i].S = (sigma_crisp *)get_s_crisp(pop->ind[i].obj[0] * -1);

    	//initializePopulationDeterministic(&(pop->ind[i]));
    	for (j=0; j<nreal; j++)
		{
    		pop->ind[i].xreal[j] = rnd(0, nalternatives - 1);
		}
    }
    return;
}

void initialize_pop_k_means (int K, int parent[], int m, int Q[m][m], double d[m][m])
{
    int i, k, x;

	int L[nalternatives];
	for (i = 0; i < nalternatives; ++i) {
		L[i] = -1;
	}

	k_means2(K, nalternatives, 25, L, Q, d);

	for (k = 0; k < K; ++k)
	{
		for (i = 0; i < nalternatives; i++)
		{
			if (L[i] == k)
			{
				int index = i;

				for (x = i + 1; x < nalternatives; x++)
					if (L[x] == L[i])
					{
						parent[index] = x;
						index = x;
					}

				parent[index] = index;

				i = nalternatives;
			}
		}
	}
    return;
}

void generate_mst (int mst[], int m, int Q[m][m], double d[m][m])
{
	int v_new[nalternatives];
	int e_new[nalternatives][nalternatives];

	int i, j, u, v;
	for (i = 0; i < nalternatives; ++i)
	{
		v_new[i] = 0;
		mst[i] = -1;
		for (j = 0; j < nalternatives; ++j)
		{
			e_new[i][j] = 0;
		}
	}

	int root = rnd(0, nalternatives - 1);
	v_new[root] = 1;
	mst[root] = root;

	int min_u = -1;
	int min_v = -1;
	int count = nalternatives;

	while(count > 0)
	{
		double min_dist = INF;
		count = 0;

		for (v = 0; v < nalternatives; ++v)
		{
			if(v_new[v] == 1)
			{
				for (u = 0; u < nalternatives; ++u)
				{
					if(v_new[u] == 0)
					{
						if(d[v][u] <= min_dist)
						{
							min_dist = d[v][u];
							min_u = u;
							min_v = v;
						}
					}
				}
			}
			else
			{
				count++;
			}
		}

		e_new[min_v][min_u] = 1;
		e_new[min_u][min_v] = 1;
		v_new[min_u] = 1;
		mst[min_u] = min_v;

	}
}
