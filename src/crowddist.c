/* Crowding distance computation routines */

# include <stdio.h>
# include <stdlib.h>
# include <math.h>

# include "global.h"
# include "rand.h"

/* Routine to compute crowding distance based on ojbective function values when the population in in the form of a list */
void assign_crowding_distance_list (population *pop, list *lst, int front_size)
{
    int **obj_array;
    int *dist;
    int i, j;
    list *temp;
    temp = lst;
    if (front_size==1)
    {
        pop->ind[lst->index].crowd_dist = INF;
        return;
    }
    if (front_size==2)
    {
        pop->ind[lst->index].crowd_dist = INF;
        pop->ind[lst->child->index].crowd_dist = INF;
        return;
    }
    obj_array = (int **)malloc(nobj*sizeof(int*));
    dist = (int *)malloc(front_size*sizeof(int));
    for (i=0; i<nobj; i++)
    {
        obj_array[i] = (int *)malloc(front_size*sizeof(int));
    }
    for (j=0; j<front_size; j++)
    {
        dist[j] = temp->index;
        temp = temp->child;
    }
    assign_crowding_distance (pop, dist, obj_array, front_size);
    free (dist);
    for (i=0; i<nobj; i++)
    {
        free (obj_array[i]);
    }
    free (obj_array);
    return;
}

/* Routine to compute crowding distance based on objective function values when the population in in the form of an array */
void assign_crowding_distance_indices (population *pop, int c1, int c2)
{
    int **obj_array;
    int *dist;
    int i, j;
    int front_size;
    front_size = c2-c1+1;
    if (front_size==1)
    {
        pop->ind[c1].d_paf = INF;
        pop->ind[c1].crowd_dist = INF;
        return;
    }
    if (front_size==2)
    {
        pop->ind[c1].d_paf = INF;
        pop->ind[c2].d_paf = INF;
        pop->ind[c1].crowd_dist = INF;
		pop->ind[c2].crowd_dist = INF;
        return;
    }
    obj_array = (int **)malloc(nobj*sizeof(int*));
    dist = (int *)malloc(front_size*sizeof(int));
    for (i=0; i<nobj; i++)
    {
        obj_array[i] = (int *)malloc(front_size*sizeof(int));
    }
    for (j=0; j<front_size; j++)
    {
        dist[j] = c1++;
    }
    assign_crowding_distance (pop, dist, obj_array, front_size);
    free (dist);
    for (i=0; i<nobj; i++)
    {
        free (obj_array[i]);
    }
    free (obj_array);
    return;
}

/* Routine to compute crowding distances */
void assign_crowding_distance (population *pop, int *dist, int **obj_array, int front_size)
{

	int i, j, fap;
	int visited[front_size];
	double minCutMax, minCutMin, minimunPairWiseMax, minimunPairWiseMin;
	double distances[front_size];

	/* 0 the fixed aspiration point is used
	 * 1 crowing distance
	 * 2 reference point
	 * If 1 or 2 is used, modify sort.c lines 72 and 78; change d_paf for crowd_dist
	 * */
	fap = 2;

	if(fap == 0)
	{
		// CALCULATE D PAF

		double xshared, yshared, sigmashared, infnorm;
		for (i=0; i<nobj; i++)
		{
			for (j=0; j<front_size; j++)
			{
				obj_array[i][j] = dist[j];
			}
			quicksort_front_obj (pop, i, obj_array[i], front_size);
		}
		for (j=0; j<front_size; j++)
		{
			pop->ind[dist[j]].d_paf = 0.0;
		}
		xshared = 0;
		yshared = 0;
		for (j=0; j<front_size; j++)
		{
			xshared += pop->ind[dist[j]].obj[1];
			yshared += pop->ind[dist[j]].obj[2];
		}
		xshared = xshared / front_size;
		yshared = yshared / front_size;
		sigmashared = xshared >= yshared ? xshared : yshared;
		for (j=0; j<front_size; j++)
		{
			infnorm = pop->ind[dist[j]].obj[1] >= pop->ind[dist[j]].obj[2] ? pop->ind[dist[j]].obj[1] : pop->ind[dist[j]].obj[2];
			if (infnorm > sigmashared)
			{
				pop->ind[dist[j]].d_paf = (infnorm / sigmashared) * -1;
			}
		}

		//CALCULATE CROWING DISTANCE
		for (i=0; i<nobj; i++)
		{
			for (j=0; j<front_size; j++)
			{
				obj_array[i][j] = dist[j];
			}
			quicksort_front_obj (pop, i, obj_array[i], front_size);
		}
		for (j=0; j<front_size; j++)
		{
			pop->ind[dist[j]].crowd_dist = 0.0;
		}
		for (i=0; i<nobj; i++)
		{
			pop->ind[obj_array[i][0]].crowd_dist = INF;
		}
		for (i=0; i<nobj; i++)
		{
			for (j=1; j<front_size-1; j++)
			{
				if (pop->ind[obj_array[i][j]].crowd_dist != INF)
				{
					if (pop->ind[obj_array[i][front_size-1]].obj[i] == pop->ind[obj_array[i][0]].obj[i])
					{
						pop->ind[obj_array[i][j]].crowd_dist += 0.0;
					}
					else
					{
						pop->ind[obj_array[i][j]].crowd_dist += (pop->ind[obj_array[i][j+1]].obj[i] - pop->ind[obj_array[i][j-1]].obj[i])/(pop->ind[obj_array[i][front_size-1]].obj[i] - pop->ind[obj_array[i][0]].obj[i]);
					}
				}
			}
		}
		for (j=0; j<front_size; j++)
		{
			if (pop->ind[dist[j]].crowd_dist != INF)
			{
				pop->ind[dist[j]].crowd_dist = (pop->ind[dist[j]].crowd_dist)/2;
			}
		}
		return;

	}

	if(fap == 1)
	{
		for (i=0; i<nobj; i++)
		{
			for (j=0; j<front_size; j++)
			{
				obj_array[i][j] = dist[j];
			}
			quicksort_front_obj (pop, i, obj_array[i], front_size);
		}
		for (j=0; j<front_size; j++)
		{
			pop->ind[dist[j]].crowd_dist = 0.0;
		}
		for (i=0; i<nobj; i++)
		{
			pop->ind[obj_array[i][0]].crowd_dist = INF;
		}
		for (i=0; i<nobj; i++)
		{
			for (j=1; j<front_size-1; j++)
			{
				if (pop->ind[obj_array[i][j]].crowd_dist != INF)
				{
					if (pop->ind[obj_array[i][front_size-1]].obj[i] == pop->ind[obj_array[i][0]].obj[i])
					{
						pop->ind[obj_array[i][j]].crowd_dist += 0.0;
					}
					else
					{
						pop->ind[obj_array[i][j]].crowd_dist += (pop->ind[obj_array[i][j+1]].obj[i] - pop->ind[obj_array[i][j-1]].obj[i])/(pop->ind[obj_array[i][front_size-1]].obj[i] - pop->ind[obj_array[i][0]].obj[i]);
					}
				}
			}
		}
		for (j=0; j<front_size; j++)
		{
			if (pop->ind[dist[j]].crowd_dist != INF)
			{
				pop->ind[dist[j]].crowd_dist = (pop->ind[dist[j]].crowd_dist)/nobj;
			}
		}
		return;
	}

	if(fap == 2)
	{
		for (i=0; i<front_size; i++)
		{
			pop->ind[dist[i]].crowd_dist = 0.0;
			visited[i] = 0;
		}

		minCutMin = minimunPairWiseMin = INF;
		minCutMax =  minimunPairWiseMax = 0.0;

		// Denominator of Euclidian equation
		// Get he minimun and maximun values for objectives MinCut and MinimunPairWise
		for (i=0; i<front_size; i++)
		{
			if(pop->ind[dist[i]].obj[1] < minCutMin)
				minCutMin = pop->ind[dist[i]].obj[1];

			if(pop->ind[dist[i]].obj[1] > minCutMax)
				minCutMax = pop->ind[dist[i]].obj[1];

			if(pop->ind[dist[i]].obj[2] < minimunPairWiseMin)
				minimunPairWiseMin = pop->ind[dist[i]].obj[2];

			if(pop->ind[dist[i]].obj[2] > minimunPairWiseMax)
				minimunPairWiseMax = pop->ind[dist[i]].obj[2];
		}

		// 1.- Calculate Normalized Euclidean Distance
		for(i=0; i < front_size; i++)
		{
			distances[i] = pow((pop->ind[dist[i]].obj[1] - 0)  / (minCutMax - minCutMin), 2) + pow((pop->ind[dist[i]].obj[2] - 0)  / (minimunPairWiseMax - minimunPairWiseMin), 2);
			pop->ind[dist[i]].crowd_dist = -1 * sqrt(distances[i]);
			//distances[i] =  -1 * sqrt(distances[i]);
		}

		// 2.- Assign rank as crowing distance
		/*for (i = 0; i < front_size; ++i) {
			rank[i] = dist[i];
		}
		SortNN(front_size, distances, rank);
		for(i=0; i < front_size; i++)
		{
			pop->ind[rank[i]].crowd_dist = i + 1;
		}*/

		// 3.- Epsilon clearing strategy
		int  group;
		group = 1;
		for (i = 0; i < front_size; ++i) {
			if(!visited[i]) {
				visited[i] = group;
				for(j = i + 1; j < front_size; j++)
				{
					distances[j] = pow((pop->ind[dist[j]].obj[1] - pop->ind[dist[i]].obj[1])  / (minCutMax - minCutMin), 2) + pow((pop->ind[dist[j]].obj[2] - pop->ind[dist[i]].obj[2])  / (minimunPairWiseMax - minimunPairWiseMin), 2);
					distances[j] = sqrt(distances[j]);
				}
				for (j = i + 1; j < front_size; ++j) {
					if(!visited[j]) {
						if(distances[j] <= 0.00001 ) { // epsilon
							visited[j] = group;
						}
					}
				}
				group++;
			}
		}
		group = 1;
		int change[front_size];
		for (i = 0; i < front_size; ++i) {
			change[i] = 0;
		}
		for (i = 0; i < front_size; ++i) {
			if(visited[i])
				for (j = i + 1; j < front_size; ++j) {
					if(visited[i] == visited[j]) {
						if ((randomperc()) <= 0.5)
						{
							//pop->ind[dist[i]].crowd_dist = -1 * INF;
							visited[i] = 0;
							change[i] = 1;
						}
						else
						{
							//pop->ind[dist[j]].crowd_dist = -1 * INF;
							visited[j] = 0;
							change[j] = 1;
							change[i] = 0;
						}
					}
				}
			group++;
		}
		for (i = 0; i < front_size; ++i) {
			if(change[i] == 1)
				pop->ind[dist[i]].crowd_dist = -1 * INF;
		}
		return;
	}

	return;
}
