/* Routines for storing population data into files */

# include <stdio.h>
# include <stdlib.h>
# include <math.h>

# include "global.h"
# include "rand.h"

void report_nondominated (population *pop, FILE *fpt)
{
    int duplicates[popsize];
	int i, k;

	for (i = 0; i < popsize; ++i)
		duplicates[i] = 0;

	for (i=0; i<popsize; i++)
		for (k = i + 1; k < popsize; ++k)
			if(pop->ind[i].obj[0] == pop->ind[k].obj[0] && pop->ind[i].obj[1] == pop->ind[k].obj[1] && pop->ind[i].obj[2] == pop->ind[k].obj[2])
				duplicates[k] = 1;

    for (i=0; i<popsize; i++)
    {
        if(pop->ind[i].rank==1 && duplicates[i] == 0)
        {
            fprintf(fpt,"%lf %lf %lf\n", pop->ind[i].obj[0], pop->ind[i].obj[1], pop->ind[i].obj[2]);
        }
    }
    return;
}

void report_decode (population *pop, FILE *fpt)
{
	int duplicates[popsize];
	int i, j, k;

	for (i = 0; i < popsize; ++i)
		duplicates[i] = 0;

	for (i=0; i<popsize; i++)
		for (k = i + 1; k < popsize; ++k)
			if(pop->ind[i].obj[0] == pop->ind[k].obj[0] && pop->ind[i].obj[1] == pop->ind[k].obj[1] && pop->ind[i].obj[2] == pop->ind[k].obj[2])
				duplicates[k] = 1;


	    for (i=0; i<popsize; i++)
	    {
	    	//if(pop->ind[i].rank==1 && duplicates[i]==0)
	    	if(pop->ind[i].rank==1 && duplicates[i] == 0)
	    	{
				int M;
				int S[nalternatives][nalternatives];
				int P[nalternatives];

				calculate_preferenceMatrix(pop->ind[i].obj[0] * -1, nalternatives, S);
				M = createPartitions(P, nalternatives, pop->ind[i].xreal);
				int L[M][M];

				deduceRelations(P, nalternatives, S, M, L);

				/*// Incoming Flow
				int IF[M];
				for (j = 0; j < M; ++j) {
					IF[j] = 0;
					for (k = 0; k < M; ++k) {
							IF[j] += L[j][k];
					}
				}
				// Outgoing Flow
				int OF[M];
				for (j = 0; j < M; ++j) {
					OF[j] = 0;
					for (k = 0; k < M; ++k) {
							OF[j] += L[k][j];
					}
				}
				// Net Flow
				int NF[M];
				for (j = 0; j < M; ++j) {
					NF[j] = IF[j] - OF[j];
				}

				int final_order[M][M];
				for (j = 0; j < M; ++j) {
					for (k = 0; k < M; ++k) {
						if(NF[j] >= NF[k])
							final_order[j][k] = 1;
						else
							final_order[j][k] = 0;
					}
				}*/

				// LAST STEP
				int rank[M];
				for (j = 0; j < M; ++j) {
					rank[j] = 0;
					for (k = 0; k < M; ++k) {
						if(L[j][k] == 0 && L[k][j] == 1)
							rank[j]++;
					}
				}

				int final_order[M][M];
				for (j = 0; j < M; ++j) {
					for (k = 0; k < M; ++k) {
						if(rank[j] < rank[k])
							final_order[j][k] = 1;
						else
							final_order[j][k] = 0;
					}
				}




				for (j=0; j<nobj; j++)
				{
					if(j==0)
						fprintf(fpt,"%lf\t",pop->ind[i].obj[j] * -1);
					else
						fprintf(fpt,"%lf\t",pop->ind[i].obj[j]);
				}

				fprintf(fpt,"%d", M);
				fprintf(fpt,"\n");

				for (j = 0; j < nalternatives; ++j)
				{
					fprintf(fpt,"%d\t", P[j]);
				}

				fprintf(fpt,"\n");
				for (j = 0; j < M; ++j)
				{
					for (k = 0; k < M; ++k)
					{
						fprintf(fpt,"%d\t", final_order[j][k]);
					}
					fprintf(fpt,"\n");
				}
				fprintf(fpt,"*\n");
	    	}
		}
}
