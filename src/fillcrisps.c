#include <stdio.h>
#include <stdlib.h>

#include "global.h"

void fill_crisp()
{
	int i, j, k;

	S_crisp = (sigma_crisp*)malloc(lambda_set_size * sizeof(sigma_crisp));
	printf("\n");
	for (i = 0; i < lambda_set_size; ++i)
	{
		S_crisp[i].lambda = lambdaset[i];
		S_crisp[i].Q = (int **)malloc(nalternatives * sizeof(int*));

		for (j = 0; j < nalternatives; ++j)
		{
			S_crisp[i].Q[j] = (int *)malloc(nalternatives * sizeof(int));
			for (k = 0; k < nalternatives; ++k)
			{
				if (Sigma[j][k] >= S_crisp[i].lambda && Sigma[k][j] >= S_crisp[i].lambda)
				{
					S_crisp[i].Q[j][k] = 1;
				}
				else if (Sigma[j][k] >= S_crisp[i].lambda && Sigma[k][j] <= S_crisp[i].lambda)
				{
					S_crisp[i].Q[j][k] = 1;
				}
				else if (Sigma[j][k] <= S_crisp[i].lambda && Sigma[k][j] >= S_crisp[i].lambda)
				{
					S_crisp[i].Q[j][k] = 0;
				}
				else
				{
					S_crisp[i].Q[j][k] = 0;
				}
			}
		}
	}
}

void fill_dist()
{
	int i, j, k, s;
	double sum = 0;
	double c = nalternatives;

	for (i = 0; i < lambda_set_size; ++i)
	{
		S_crisp[i].lambda = lambdaset[i];
		S_crisp[i].d = (double **)malloc(nalternatives * sizeof(double*));

		for (j = 0; j < nalternatives; ++j)
		{
			S_crisp[i].d[j] = (double *)malloc(nalternatives * sizeof(double));
			for (k = 0; k < nalternatives; ++k)
			{
				for (s = 0; s < nalternatives; ++s)
				{
					if ((S_crisp[i].Q[j][s] == S_crisp[i].Q[k][s]) && (S_crisp[i].Q[s][j] == S_crisp[i].Q[s][k]))
						sum++;
				}
				S_crisp[i].d[j][k] = 1 - (1 / c) * sum;

				sum = 0;
			}
		}
	}
}

void fill_NN()
{
	int i, j, k, l;

	for (i = 0; i < lambda_set_size; ++i)
	{
		S_crisp[i].lambda = lambdaset[i];
		S_crisp[i].NN = (int **)malloc(nalternatives * sizeof(int*));

		for (j = 0; j < nalternatives; ++j)
		{
			S_crisp[i].NN[j] = (int *)malloc(nalternatives * sizeof(int));
			double dist1[nalternatives];

			for (k = 0; k < nalternatives; k++)
			{
				dist1[k] = S_crisp[i].d[j][k];
			}

			int sortedNN[nalternatives];
			SortNN(nalternatives, dist1, sortedNN);

			// TIP FOR THE FIRST ITEM: Si hay 2 alternativas o mas con distancia = 0
			// puede provocar que la primera alternativa en la fila no sea ella misma
			if (sortedNN[0] != j)
			{
				// buscar el indice donde esta ubicado
				int indice = -1;
				for (k = 0; k < nalternatives; k++)
					if (sortedNN[k] == j)
					{
						indice = k;
						break;
					}

				// SWAP con el primer elemento
				int temp = sortedNN[0];
				sortedNN[0] = sortedNN[indice];
				sortedNN[indice] = temp;

			}

			for (k = 0; k < nalternatives; k++)
			{
				S_crisp[i].NN[j][k] = sortedNN[k];
			}
		}
	}
}


sigma_crisp* get_s_crisp(double l)
{
	int i;
	for (i = 0; i < lambda_set_size; ++i)
	{
		if(S_crisp[i].lambda == l)
			return &S_crisp[i];
	}
	return 0;
}

void deallocate_crisp()
{
	int i, j;
	for (i=0;i<lambda_set_size;i++)
	{
		for (j = 0; j < nalternatives; ++j)
		{
			free(S_crisp[i].Q[j]);
			free(S_crisp[i].d[j]);
			free(S_crisp[i].NN[j]);
		}
		free(S_crisp[i].Q);
		free(S_crisp[i].d);
		free(S_crisp[i].NN);
	}

	free(S_crisp);
}
