# include <stdio.h>
# include <stdlib.h>
# include <math.h>

# include "global.h"
# include "rand.h"


void k_means(int K, int m, int t, int L[], int V[m][2])
{
	int i, j, k;

	// cluster's vector
	int C[K][2];

	// Permutation vector
	int p[m];

	// Initialize vector
	for (i = 0; i < m; i++)
	{
		p[i] = i;
	}

	for (i = 0; i < m; i++)
	{
		int a = rnd(0, m - 1);
		int g1 = p[i];
		int g2 = p[a];

		p[i] = g2;
		p[a] = g1;
	}

	// Initial random centroids
	for (i = 0; i < K; i++)
	{
		C[i][0] = V[p[i]][0];
		C[i][1] = V[p[i]][1];
	}

	// Initial Clusters
	for (i = 0; i < m; ++i)
	{
		double min_distance = INF;
		for (k = 0; k < K; k++)
		{
			double distance = 0;
			for (j = 0; j < 2; ++j) {
				distance += pow(V[i][j] - C[k][j], 2);
			}
			distance = sqrt(distance);
			if (distance < min_distance)
			{
				min_distance = distance;
				L[i] = k;
			}
		}
	}

	int iter = 0;

	while(iter < t)
	{
		int x, y, count ;
		// Update Clusters
		for (k = 0; k < K; ++k)
		{
			x = y = count = 0;
			for (i = 0; i < m; ++i)
			{
				if(L[i] == k)
				{
					x += V[i][0];
					y += V[i][1];
					count++;
				}
			}
			if(count > 0)
			{
				C[k][0] = x / count;
				C[k][1] = y / count;
			}
		}

		// Find closest centroid
		for (i = 0; i < m; ++i)
		{
			double min_distance = INF;
			for (k = 0; k < K; k++)
			{
				// Calculate distance
				double distance = 0;
				for (j = 0; j < 2; ++j)
				{
					distance += pow(V[i][j] - C[k][j], 2);
				}
				distance = sqrt(distance);

				if (distance < min_distance)
				{
				   min_distance = distance;
				   L[i] = k;
				}
			}
		}
		iter++;
	}
}

void k_means2(int K, int m, int t, int L[], int Q[m][m], double d[m][m])
{
	int i, j, k;

	// cluster's vector
	int C[K];

	// Permutation vector
	int p[m];

	// Initialize vector
	for (i = 0; i < m; i++)
	{
		p[i] = i;
	}

	for (i = 0; i < m; i++)
	{
		int a = rnd(0, m - 1);
		int g1 = p[i];
		int g2 = p[a];

		p[i] = g2;
		p[a] = g1;
	}

	// Initial random centroids
	for (i = 0; i < K; i++)
	{
		C[i] = p[i];
	}

	// Initial Clusters
	for (i = 0; i < m; i++)
	{
		//min distance to centroids
		double min = INF;
		for (k = 0; k < K; k++)
		{
			if (d[i][C[k]] < min)
			{
				min = d[i][C[k]];
				L[i] = k;
			}
		}
	}

	int iter = 0;

	while (iter < t)
	{
		int perfilesCentroides[K][m];
		// Update Clusters
		for (k = 0; k < K; k++)
		{
			// UPDATE CENTROID
			for (j = 0; j < m; j++)
			{
				int suma[3] = { 0 };
				for (i = 0; i < m; i++)
				{
					if (L[i] == k) // Si esta en el cluster
					{
						if(Q[i][j] == 1 &&  Q[j][i] == 1)
						{
							suma[0]++;
						}
						else if(Q[i][j] == 1 &&  Q[j][i] == 0)
						{
							suma[0]++;
						}
						else if(Q[i][j] == 0 &&  Q[j][i] == 1)
						{
							suma[1]++;
						}
						else
						{
							suma[2]++;
						}
					}
				}

				int max = 0;
				int relacion = 0;
				int x = 0;

				for ( x = 2; x > -1; x--)
				{
					if (suma[x] >= max)
					{
						max = suma[x];
						relacion = x;
					}
				}
				perfilesCentroides[k][j] = relacion;
			}
		}

		// assign cluster
		for (i = 0; i < m; i++)
		{
			//min distance to centroids
			double min_distance = INF;
			for (k = 0; k < K; k++)
			{
				/// Obtiene la distancia entre la alternativa y el centroide
				double distance = 0;
				double suma = 0;
				for (j = 0; j < m; j++)
				{
					int preferencia = 0;
					if(Q[i][j] == 1 &&  Q[j][i] == 1)
					{
						preferencia = 0;
					}
					else if(Q[i][j] == 1 &&  Q[j][i] == 0)
					{
						preferencia = 0;
					}
					else if(Q[i][j] == 0 &&  Q[j][i] == 1)
					{
						preferencia = 1;
					}
					else
					{
						preferencia = 2;
					}
					if (preferencia == perfilesCentroides[k][j])
						suma++;
				}

				distance = 1 - (1 / (double)m) * suma;

				if (distance < min_distance)
				{
					min_distance = distance;
					L[i] = k;
				}
			}
		}
		iter++;
	}
}
