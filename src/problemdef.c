/* Test problem definitions */

# include <stdio.h>
# include <stdlib.h>
# include <math.h>

# include "global.h"
# include "rand.h"
# include "queue.h"
# include "bool.h"

# define mcda

#ifdef mcda
void test_problem (double *xreal, double *xbin, int **gene, double *obj, double *constr, sigma_crisp *S_crisp)
{
	int P[nalternatives], M;

	M = 0;
	M = createPartitions(P, nalternatives, xreal);
	int L[M][M];
	deduceRelations2(P, nalternatives, M, L, S_crisp);
	obj[1] = calculate3F(nalternatives, P, M, S_crisp);
	obj[2] = calculate3U(nalternatives, M, L, P, S_crisp);

	return;
}
#endif

void test_problem2 (double *xreal, double *obj, sigma_crisp *s_crisp, individual *ind)
{
	int P[nalternatives], M;
	M = 0;
	M = createPartitions(P, nalternatives, xreal);
	int L[M][M];
	deduceRelations2(P, nalternatives, M, L, s_crisp);
	obj[1] = calculate3F(nalternatives, P, M, s_crisp);
	obj[2] = calculate3U(nalternatives, M, L, P, s_crisp);

	return;
}

void get_partitions (double *xreal, int P[], double *obj)
{
	int S[nalternatives][nalternatives];
	calculate_preferenceMatrix(obj[0] * -1, nalternatives, S);
	createPartitions(P, nalternatives, xreal);

	return;
}

double calculate_lambda(double lowNum, double hiNum)
{
	return ((hiNum - lowNum) * ( (double)rand() / (double)RAND_MAX) + lowNum) * -1;
}
double get_lambda(double lowNum, double hiNum)
{
	int i;
	for (i = 0; i < lambda_set_size; ++i) {
		if(lambdaset[i] > hiNum)
			break;
	}

	int l = 0;

	if(i > 0)
		l = rnd(0, i - 1);

	return lambdaset[l]  * -1;
}
double get_min_lambda()
{
	int i;
	for (i = 0; i < lambda_set_size; ++i) {
		if(lambdaset[i] >= lmin  * -1)
			break;
	}

	return lambdaset[i]  * -1;
}
double get_next_lambda(double l)
{
	int i;
	for (i = 0; i < lambda_set_size; ++i) {
		if(lambdaset[i] > l * -1)
			break;
	}

	return lambdaset[i] * -1;
}
void calculate_preferenceMatrix(double l, int m, int S[][m]) {

	int i, j;
	for (i = 0; i < m; i++)
	{
		for (j = i; j < m; j++)
		{
			if (Sigma[i][j] >= l && Sigma[j][i] >= l)
			{
				S[i][j] = 1;
				S[j][i] = 1;
			}
			else if (Sigma[i][j] >= l && Sigma[j][i] <= l)
			{
				S[i][j] = 1;
				S[j][i] = 0;
			}
			else if (Sigma[i][j] <= l && Sigma[j][i] >= l)
			{
				S[i][j] = 0;
				S[j][i] = 1;
			}
			else
			{
				S[i][j] = 0;
				S[j][i] = 0;
			}
		}
	}

	return;
}

int createPartitions(int P[], int m, double *xreal)
{
	int i, current_cluster = 0, neighbour, previous[m];

	for (i = 0; i < m; i++)
	{
		P[i] = -1;
	}

	for (i = 0; i < m; i++)
	{
		int ctr = 0;

		if (P[i] == -1)
		{
			P[i] = current_cluster;
			neighbour = xreal[i];
			previous[ctr] = i;
			ctr++;

			while (P[neighbour] == -1)
			{
				previous[ctr] = neighbour;
				P[neighbour] = current_cluster;
				neighbour = xreal[neighbour];
				ctr++;
			}

			if (P[neighbour] != current_cluster)
			{
				ctr--;

				while (ctr >= 0)
				{
					P[previous[ctr]] = P[neighbour];
					ctr--;
				}
			}
			else
			{
				current_cluster++;
			}
		}
	}

	return current_cluster;
}

void deduceRelations(int P[], int m, int S[][m], int M, int L[][M])
{
	int r, q, i, j;
	for (r = 0; r < M; r++)
	{
		for (q = r; q < M; q++)
		{
			// If is the same cluster, it is an Indifference relation
			if (r == q)
			{
				L[r][q] = 1;
			}
			else
			{
				int sum[3] = { 0 };
				for (i = 0; i < m; i++)
				{
					if (P[i] == r)
					{
						for (j = 0; j < m; j++)
						{
							if (P[j] == q)
							{
								if(S[i][j] == 1 &&  S[j][i] == 1)
								{
									sum[0]++;
								}
								else if(S[i][j] == 1 &&  S[j][i] == 0)
								{
									sum[0]++;
								}
								else if(S[i][j] == 0 &&  S[j][i] == 1)
								{
									sum[1]++;
								}
								else
								{
									sum[2]++;
								}
							}
						}
					}
				}
				int c = 0;
				if (sum[2] >= c)
				{
					L[r][q] = 0;
					L[q][r] = 0;
					c = sum[2];
				}
				if (sum[1] >= c)
				{
					L[r][q] = 0;
					L[q][r] = 1;
					c = sum[1];
				}
				if (sum[0] >= c)
				{
					L[r][q] = 1;
					L[q][r] = 0;
					c = sum[0];
				}
			}
		}
	}
	return;
}
void deduceRelations2(int P[], int m, int M, int L[][M], sigma_crisp *S_crisp)
{
	int r, q, i, j;
	for (r = 0; r < M; r++)
	{
		for (q = r; q < M; q++)
		{
			// If is the same cluster, it is an Indifference relation
			if (r == q)
			{
				L[r][q] = 1;
			}
			else
			{
				int sum[3] = { 0 };
				for (i = 0; i < m; i++)
				{
					if (P[i] == r)
					{
						for (j = 0; j < m; j++)
						{
							if (P[j] == q)
							{
								if(S_crisp->Q[i][j] == 1 &&  S_crisp->Q[j][i] == 1)
								{
									sum[0]++;
								}
								else if(S_crisp->Q[i][j] == 1 &&  S_crisp->Q[j][i] == 0)
								{
									sum[0]++;
								}
								else if(S_crisp->Q[i][j] == 0 &&  S_crisp->Q[j][i] == 1)
								{
									sum[1]++;
								}
								else
								{
									sum[2]++;
								}
							}
						}
					}
				}
				int c = 0;
				if (sum[2] >= c)
				{
					L[r][q] = 0;
					L[q][r] = 0;
					c = sum[2];
				}
				if (sum[1] >= c)
				{
					L[r][q] = 0;
					L[q][r] = 1;
					c = sum[1];
				}
				if (sum[0] >= c)
				{
					L[r][q] = 1;
					L[q][r] = 0;
					c = sum[0];
				}
			}
		}
	}
	return;
}
void deduceRelations3(int P[], int m, int M, int L[][M], double l)
{
	int r, q, i, j;
	for (r = 0; r < M; r++)
	{
		for (q = r; q < M; q++)
		{
			// If is the same cluster, it is an Indifference relation
			if (r == q)
			{
				L[r][q] = 1;
			}
			else
			{
				int sum[3] = { 0 };
				for (i = 0; i < m; i++)
				{
					if (P[i] == r)
					{
						for (j = 0; j < m; j++)
						{
							if (P[j] == q)
							{
								if(Sigma[i][j] >= l &&  Sigma[j][i] >= l)
								{
									sum[0]++;
								}
								else if(Sigma[i][j] >= l &&  Sigma[j][i] < l)
								{
									sum[0]++;
								}
								else if(Sigma[i][j] < l &&  Sigma[j][i] >= l)
								{
									sum[1]++;
								}
								else
								{
									sum[2]++;
								}
							}
						}
					}
				}
				int c = 0;
				if (sum[2] >= c)
				{
					L[r][q] = 0;
					L[q][r] = 0;
					c = sum[2];
				}
				if (sum[1] >= c)
				{
					L[r][q] = 0;
					L[q][r] = 1;
					c = sum[1];
				}
				if (sum[0] >= c)
				{
					L[r][q] = 1;
					L[q][r] = 0;
					c = sum[0];
				}
			}
		}
	}
	return;
}
int calculateF(int m, int P[], int S[][m], int M)
{
	int q, i, j, sum = 0;
	for (q = 0; q < M; q++)
	{
		for (i = 0; i < m; i++)
		{
			for (j = i; j < m; j++)
			{
				if (P[i] == q && P[j] == q)
				{
					if (!(S[i][j] == 1 && S[j][i] == 1))
					{
						sum++;
					}
				}
			}
		}
	}
	return sum;
}
int calculate2F(int m, int P[], int M, double l)
{
	int q, i, j, sum = 0;
	for (q = 0; q < M; q++)
	{
		for (i = 0; i < m; i++)
		{
			for (j = i; j < m; j++)
			{
				if (P[i] == q && P[j] == q)
				{
					if (!(Sigma[i][j] >= l && Sigma[j][i] >= l))
					{
						sum++;
					}
				}
			}
		}
	}
	return sum;
}
int calculate3F(int m, int P[], int M, sigma_crisp *S_crisp)
{
	int q, i, j, sum = 0;
	for (q = 0; q < M; q++)
	{
		for (i = 0; i < m; i++)
		{
			for (j = i; j < m; j++)
			{
				if (P[i] == q && P[j] == q)
				{
					if (!(S_crisp->Q[i][j] == 1 && S_crisp->Q[j][i] == 1))
					{
						sum++;
					}
				}
			}
		}
	}
	return sum;
}
int calculateU(int m, int M, int L[][M], int P[], int S[][m])
{
	int i, j, r, s;
	/// If there is just one class, it has no inconsistencies
	if (M == 1)
	{
		return 0;
	}
	int sum = 0;
	for (i = 0; i < M; i++)
	{
		for (j = i; j < M; j++)
		{
			if (i != j)
			{
				for (r = 0; r < m; r++)
				{
					if (P[r] == i)
					{
						for (s = 0; s < m; s++)
						{
							if (r != s)
							{
								if (P[s] == j)
								{
									if (!(S[r][s] == L[i][j] && S[s][r] == L[j][i]))
									{
										sum++;
									}
								}
							}
						}
					}
				}
			}
		}
	}
	return sum;
}
int calculate2U(int m, int M, int L[][M], int P[], double l)
{
	int i, j, r, s;
	/// If there is just one class, it has no inconsistencies
	if (M == 1)
	{
		return 0;
	}
	int sum = 0;
	for (i = 0; i < M; i++)
	{
		for (j = i; j < M; j++)
		{
			if (i != j)
			{
				for (r = 0; r < m; r++)
				{
					if (P[r] == i)
					{
						for (s = 0; s < m; s++)
						{
							if (r != s)
							{
								if (P[s] == j)
								{
									if (!((Sigma[r][s] >= l) == L[i][j] && (Sigma[s][r] >= l) == L[j][i]))
									{
										sum++;
									}
								}
							}
						}
					}
				}
			}
		}
	}
	return sum;
}

int calculate3U(int m, int M, int L[][M], int P[], sigma_crisp *S_crisp)
{
	int i, j, r, s;
		/// If there is just one class, it has no inconsistencies
		if (M == 1)
		{
			return 0;
		}
		int sum = 0;
		for (i = 0; i < M; i++)
		{
			for (j = i; j < M; j++)
			{
				if (i != j)
				{
					for (r = 0; r < m; r++)
					{
						if (P[r] == i)
						{
							for (s = 0; s < m; s++)
							{
								if (r != s)
								{
									if (P[s] == j)
									{
										if (!(S_crisp->Q[r][s] == L[i][j] && S_crisp->Q[s][r] == L[j][i]))
										{
											sum++;
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return sum;
}

void distance_matrix(int m, int Q[][m], double d[][m])
{
	int i, j, s;
	double sum = 0;
	double c = m;

	for (i = 0; i < m; i++)
	{
		for (j = i; j < m; j++)
		{
			for (s = 0; s < m; s++)
			{
				if ((Q[i][s] == Q[j][s]) && (Q[s][i] == Q[s][j]))
					sum++;
			}

			d[i][j] = 1 - (1 / c) * sum;

			d[j][i] = d[i][j];

			sum = 0;
		}
	}
}

void distance_matrix2(int m, int Q[][m], double d[][m], double l)
{
	int i, j, s;
	double sum = 0;
	double c = m;

	for (i = 0; i < m; i++)
	{
		for (j = i; j < m; j++)
		{
			for (s = 0; s < m; s++)
			{
				if ((Sigma[i][s] >= l && Sigma[j][s] >= l) && (Sigma[s][i] >=l && Sigma[s][j] >= l))
					sum++;
			}

			d[i][j] = 1 - (1 / c) * sum;

			d[j][i] = d[i][j];

			sum = 0;
		}
	}
}

int CalculatesR(int m, int Q[][m])
{
	int i, j, sum = 0;
	for (i = 0; i < m; i++)
	{
		for (j = 0; j < m; j++)
		{
			if (Q[i][j] == 0 && Q[j][i] == 0)
				sum++;
		}
	}
	return sum;
}

int Calculates2R(int m, sigma_crisp *S_crisp)
{
	int i, j, sum = 0;
	for (i = 0; i < m; i++)
	{
		for (j = 0; j < m; j++)
		{
			if (S_crisp->Q[i][j] == 0 && S_crisp->Q[j][i] == 0)
				sum++;
		}
	}
	return sum;
}

void nearest_neighbours(int m, int NN[m][m], double d[m][m])
{
	int i, j;
	for (i = 0; i < m; i++)
	{
		double dist1[m];

		for (j = 0; j < m; j++)
		{
			dist1[j] = d[i][j];
		}

		int sortedNN[m];
		SortNN(nalternatives, dist1, sortedNN);

		// TIP FOR THE FIRST ITEM: Si hay 2 alternativas o mas con distancia = 0
		// puede provocar que la primera alternativa en la fila no sea ella misma
		if (sortedNN[0] != i)
		{
			// buscar el indice donde esta ubicado
			int indice = -1;
			for (j = 0; j < m; j++)
				if (sortedNN[j] == i)
				{
					indice = j;
					break;
				}

			// SWAP con el primer elemento
			int temp = sortedNN[0];
			sortedNN[0] = sortedNN[indice];
			sortedNN[indice] = temp;

		}

		for (j = 0; j < m; j++)
		{
			NN[i][j] = sortedNN[j];
		}
	}
}

void nearest_neighbours2(int m, int NN[m][m], sigma_crisp *S_crisp)
{
	int i, j;
	for (i = 0; i < m; i++)
	{
		double dist1[m];

		for (j = 0; j < m; j++)
		{
			dist1[j] = S_crisp->d[i][j];
		}

		int sortedNN[m];
		SortNN(nalternatives, dist1, sortedNN);

		// TIP FOR THE FIRST ITEM: Si hay 2 alternativas o mas con distancia = 0
		// puede provocar que la primera alternativa en la fila no sea ella misma
		if (sortedNN[0] != i)
		{
			// buscar el indice donde esta ubicado
			int indice = -1;
			for (j = 0; j < m; j++)
				if (sortedNN[j] == i)
				{
					indice = j;
					break;
				}

			// SWAP con el primer elemento
			int temp = sortedNN[0];
			sortedNN[0] = sortedNN[indice];
			sortedNN[indice] = temp;

		}

		for (j = 0; j < m; j++)
		{
			NN[i][j] = sortedNN[j];
		}
	}
}

void SortNN(int m, double d[], int sorted[])
{
	int i, j; /* counters */
	int min; /* index of minimum */
	double r[m];

	for (i = 0; i < m; i++)
	{
		r[i] = d[i];
		sorted[i] = i;
	}

	for (i = 0; i < m; i++)
	{
		min = i;
		for (j = i + 1; j < m; j++)
			if (r[j] < r[min]) min = j;

		// SWAP
		double temp = r[i];
		r[i] = r[min];
		r[min] = temp;

		int temp2 = sorted[i];
		sorted[i] = sorted[min];
		sorted[min] = temp2;
	}
}
