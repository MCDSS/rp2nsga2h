/* Routine for evaluating population members  */

# include <stdio.h>
# include <stdlib.h>
# include <math.h>

# include "global.h"
# include "rand.h"

/* Routine to evaluate objective function values and constraints for a population */
void evaluate_pop (population *pop)
{
    int i;
    for (i=0; i<popsize; i++)
    {
        evaluate_ind (&(pop->ind[i]));
    }
    return;
}

/* Routine to evaluate objective function values and constraints for an individual */
void evaluate_ind (individual *ind)
{
    test_problem2 (ind->xreal, ind->obj, ind->S, ind);

    return;
}

int verifyOptimal(population *pop, int generations) {
    int i;
    for (i=0; i<popsize; i++)
    {
    	if(pop->ind[i].obj[1] == 0 && pop->ind[i].obj[2] == 0)
    	{
    		FILE *fp;
			fp=fopen("RecordsFound.txt", "a+");
			fprintf(fp,"1\t %d\t %lf\t %lf\t %lf\t", generations, pop->ind[i].obj[0], pop->ind[i].obj[1], pop->ind[i].obj[2]);
			fflush(fp);
			fclose(fp);
    		return 1;
    	}
    }
    return 0;
}
