/* Crossover routines */

# include <stdio.h>
# include <stdlib.h>
# include <math.h>

# include "global.h"
# include "rand.h"

/* Function to cross two individuals */
void crossover (individual *parent1, individual *parent2, individual *child1, individual *child2)
{
	int i;

	if (randomperc() <= pcross_real)
	{
		for (i=0; i<nreal; i++)
		{

				if (randomperc()<=0.5 )
				{
					child1->xreal[i] = parent1->xreal[i];
					child2->xreal[i] = parent2->xreal[i];
				}
				else
				{
					child1->xreal[i] = parent2->xreal[i];
					child2->xreal[i] = parent1->xreal[i];
				}
			}
	}
	else
	{
		for (i=0; i<nreal; i++)
		{
			child1->xreal[i] = parent1->xreal[i];
			child2->xreal[i] = parent2->xreal[i];
		}
	}

    return;
}
