/* Tournamenet Selections routines */

# include <stdio.h>
# include <stdlib.h>
# include <math.h>

# include "global.h"
# include "rand.h"

individual* cs_tournament (individual *ind1, individual *ind2, int obj);

/* Routine for tournament selection, it creates a new_pop from old_pop by performing tournament selection and the crossover */
void selection (population *old_pop, population *new_pop)
{
    int *a1, *a2;
    int temp;
    int i;
    int rand;
    individual *parent1, *parent2;
    a1 = (int *)malloc(popsize*sizeof(int));
    a2 = (int *)malloc(popsize*sizeof(int));
    for (i=0; i<popsize; i++)
    {
        a1[i] = a2[i] = i;
    }
    for (i=0; i<popsize; i++)
    {
        rand = rnd (i, popsize-1);
        temp = a1[rand];
        a1[rand] = a1[i];
        a1[i] = temp;
        rand = rnd (i, popsize-1);
        temp = a2[rand];
        a2[rand] = a2[i];
        a2[i] = temp;
    }
    for (i=0; i<popsize; i+=4)
    {
        parent1 = tournament (&old_pop->ind[a1[i]], &old_pop->ind[a1[i+1]]);
        parent2 = tournament (&old_pop->ind[a1[i+2]], &old_pop->ind[a1[i+3]]);
        crossover (parent1, parent2, &new_pop->ind[i], &new_pop->ind[i+1]);
        new_pop->ind[i].obj[0] = get_lambda(lmin, lmax);
        new_pop->ind[i+1].obj[0] = get_lambda(lmin, lmax);
        new_pop->ind[i].S = (sigma_crisp *)get_s_crisp(new_pop->ind[i].obj[0] * -1);
		new_pop->ind[i+1].S = (sigma_crisp *)get_s_crisp(new_pop->ind[i+1].obj[0] * -1);
        parent1 = tournament (&old_pop->ind[a2[i]], &old_pop->ind[a2[i+1]]);
        parent2 = tournament (&old_pop->ind[a2[i+2]], &old_pop->ind[a2[i+3]]);
        crossover (parent1, parent2, &new_pop->ind[i+2], &new_pop->ind[i+3]);
        new_pop->ind[i+2].obj[0] = get_lambda(lmin, lmax);
        new_pop->ind[i+3].obj[0] = get_lambda(lmin, lmax);
        new_pop->ind[i+2].S = (sigma_crisp *)get_s_crisp(new_pop->ind[i+2].obj[0] * -1);
		new_pop->ind[i+3].S = (sigma_crisp *)get_s_crisp(new_pop->ind[i+3].obj[0] * -1);
    }
    free (a1);
    free (a2);
    return;
}

/* Routine for binary tournament */
individual* tournament (individual *ind1, individual *ind2)
{
    int flag;
    flag = check_dominance (ind1, ind2);
    if (flag==1)
    {
        return (ind1);
    }
    if (flag==-1)
    {
        return (ind2);
    }
    if (ind1->d_paf > ind2->d_paf)
    {
        return(ind1);
    }
    if (ind2->d_paf > ind1->d_paf)
    {
        return(ind1);
    }
    if (ind1->crowd_dist > ind2->crowd_dist)
    {
        return(ind1);
    }
    if (ind2->crowd_dist > ind1->crowd_dist)
    {
        return(ind2);
    }
    if ((randomperc()) <= 0.5)
    {
        return(ind1);
    }
    else
    {
        return(ind2);
    }
}

individual* cs_tournament (individual *ind1, individual *ind2, int obj)
{
	if (ind1->obj[obj] < ind2->obj[obj])
		return (ind1);
	else if (ind1->obj[obj] > ind2->obj[obj])
		return (ind2);
	else
	{
		if (ind1->obj[0] >= ind1->obj[0])
			return (ind1);
		else
			return (ind2);
	}
}
