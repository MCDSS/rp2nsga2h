#include <stdio.h>
#include <stdlib.h>

#include "global.h"

void selection_sort(double *s, int n);
void swap( double *num1, double *num2);

void discretizer()
{
	//double * lambdaset;
	lambda_set_size = 1;
	lambdaset = (double *)malloc(sizeof(double));
	lambdaset[0] = 1;

	int i, j, k, b;
	for (i = 0; i < nalternatives; i++)
	{
		for (j = 0; j < nalternatives; j++)
		{
			b = 0;
			for(k = 0; k < lambda_set_size; k++)
			{
				if ((Sigma[i][j] < lmin || Sigma[i][j] > lmax ) || Sigma[i][j] == lambdaset[k])
				{
					b = 1;
					break;
				}
			}
			if(!b)
			{
				lambda_set_size++;
				lambdaset = (double *)realloc(lambdaset,(lambda_set_size)*sizeof(double));
				lambdaset[lambda_set_size - 1] = Sigma[i][j];
			}
		}
	}
	selection_sort(lambdaset, lambda_set_size);
}

void selection_sort(double *s, int n)
{
	int i,j; /* counters */
	int min; /* index of minimum */
	for (i=0; i<n; i++) {
		min=i;
		for (j=i+1; j<n; j++)
			if (s[j] < s[min]) min=j;
		swap(&s[i],&s[min]);
	}
}

void swap( double *num1, double *num2)
{
	double temp ;
    temp  = *num1 ;
    *num1 = *num2 ;
    *num2 = temp ;
}

void deallocate_lambdaset()
{
	free(lambdaset);
}
