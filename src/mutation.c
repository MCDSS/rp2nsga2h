/* Mutation routines */

# include <stdio.h>
# include <stdlib.h>
# include <math.h>

# include "global.h"
# include "rand.h"

/* Function to perform mutation in a population */
void mutation_pop (population *pop)
{
	// UNIFORM MUTATION
    int i, j;
    for (i=0; i<popsize; i++)
    {
    	for (j = 0; j < nreal; ++j)
		{
			if (randomperc() <= (double)1/nalternatives )
			{
				pop->ind[i].xreal[j] = rnd(0, nalternatives - 1);
			}
		}
    }
    return;
}

/* Dynamic L value*/
void mutation_pop3 (population *pop)
{
    int i, j, l;

    for (i=0; i<popsize; i++)
    {
    	int L[nalternatives];
    	for (j = 0; j < nalternatives; ++j)
    	{
    		L[j] = 0;
		}

    	for (j = 0; j < nalternatives; ++j)
    	{
    		for (l = 1; l < nalternatives; ++l)
    		{
    			if(pop->ind[i].S->Q[j][pop->ind[i].S->NN[j][l]] && pop->ind[i].S->Q[pop->ind[i].S->NN[j][l]][j])
    			{
    				L[j]++;
    			}
    			else
    			{
    				break;
    			}
			}
		}

		for (j = 0; j < nalternatives; j++)
		{
			for (l = 0; l < nalternatives; l++)
			{
				if (pop->ind[i].S->NN[j][l] == pop->ind[i].xreal[j])
				{
					break;
				}
			}

			double prob = randomperc();

			if (prob <= (1.0 / (double)nalternatives) + ( ((double)l / (double)nalternatives) * ((double)l / (double)nalternatives)) )
			{
				pop->ind[i].xreal[j] = pop->ind[i].S->NN[j][rnd(0, L[j])];
			}
		}
    }
    return;
}

/* Nearest Neighbours Original with L parameter*/
void mutation_pop3_1 (population *pop)
{
    int i, j, l;

    for (i=0; i<popsize; i++)
    {
    	int L;
    	L = 3;


		for (j = 0; j < nalternatives; j++)
		{
			for (l = 0; l < nalternatives; l++)
			{
				if (pop->ind[i].S->NN[j][l] == pop->ind[i].xreal[j])
				{
					break;
				}
			}

			double prob = randomperc();

			if (prob <= (1.0 / (double)nalternatives) + ( ((double)l / (double)nalternatives) * ((double)l / (double)nalternatives)) )
			{
				pop->ind[i].xreal[j] = pop->ind[i].S->NN[j][rnd(0, L)];
			}
		}
    }
    return;
}

/* Dynamic L Indifferent set */
void mutation_pop3_2 (population *pop)
{
    int i, j, k, l;

    for (i=0; i<popsize; i++)
    {
		for (j = 0; j < nalternatives; j++)
		{
			int indifferent_set_size;
			int indifferent_set[nalternatives];

			indifferent_set_size = 0;

			for (k = 0; k < nalternatives; ++k)
			{
				indifferent_set[k] = 0;
				if(pop->ind[i].S->Q[j][k] == 1 && pop->ind[i].S->Q[k][j] == 1)
				{
					indifferent_set[indifferent_set_size] = k;
					indifferent_set_size++;
				}
			}


			for (l = 0; l < nalternatives; l++)
			{
				if (pop->ind[i].S->NN[j][l] == pop->ind[i].xreal[j])
				{
					break;
				}
			}

			double prob = randomperc();

			//VALIDAR SI ES LA MISMA ALTERNATIVA

			if (prob <= (1.0 / (double)nalternatives) + ( ((double)l / (double)nalternatives) * ((double)l / (double)nalternatives)) )
			{
				pop->ind[i].xreal[j] = indifferent_set[rnd(0, indifferent_set_size)];
			}
		}
    }
    return;
}

void mutation_ind (individual *ind)
{
    int i, j;
	int Q[nalternatives][nalternatives];
	double d[nalternatives][nalternatives];
	calculate_preferenceMatrix(ind->obj[0] * -1, nalternatives, Q);
	distance_matrix(nalternatives, Q, d);

	for (i = 0; i < nalternatives; i++)
	{
		int indifferent_set_size;
		int indifferent_set[nalternatives];

		indifferent_set_size = 0;

		// REMOVE AFTER CHECK
		for (j = 0; j < nalternatives; ++j)
		{
			indifferent_set[j] = 0;
		}

		double prob = randomperc();

		// VALIDATE IF j IS NOT INDIFFERENT TO i DISTANCE = 1
		for (j = 0; j < nalternatives; ++j)
		{
			if(Q[i][j] == 1 && Q[j][i] == 1)
			{
				indifferent_set[indifferent_set_size] = j;
				indifferent_set_size++;
			}
		}

		int x;
		x = ind->xreal[i];
		if((i == x || !(Q[i][x] == 1 && Q[x][i] == 1)) || prob <= d[i][x])
		{
			// Uniform
			ind->xreal[i] = indifferent_set[rnd(0, indifferent_set_size)];

		}
	}
    return;
}
