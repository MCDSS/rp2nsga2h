/* This file contains the variable and function declarations */

# ifndef _GLOBAL_H_
# define _GLOBAL_H_

# define INF 1.0e14
# define EPS 1.0e-14
# define E  2.71828182845905
# define PI 3.14159265358979
# define GNUPLOT_COMMAND "gnuplot -persist"

typedef struct
{
	double lambda;
    int **Q;
    double **d;
    int **NN;
}
sigma_crisp;

typedef struct
{
    int rank;
    double constr_violation;
    double *xreal;
    int **gene;
    double *xbin;
    double *obj;
    double *constr;
    double crowd_dist;
    double d_paf;
    sigma_crisp *S;
}
individual;

typedef struct
{
    individual *ind;
}
population;

typedef struct lists
{
    int index;
    struct lists *parent;
    struct lists *child;
}
list;



extern int nreal;
extern int nbin;
extern int nobj;
extern int ncon;
extern int popsize;
extern double pcross_real;
extern double pcross_bin;
extern double pmut_real;
extern double pmut_bin;
extern double eta_c;
extern double eta_m;
extern int ngen;
extern int nbinmut;
extern int nrealmut;
extern int nbincross;
extern int nrealcross;
extern int *nbits;
extern double *min_realvar;
extern double *max_realvar;
extern double *min_binvar;
extern double *max_binvar;
extern int bitlength;
extern int choice;
extern int obj1;
extern int obj2;
extern int obj3;
extern int angle1;
extern int angle2;

void allocate_memory_pop (population *pop, int size);
void allocate_memory_ind (individual *ind);
void deallocate_memory_pop (population *pop, int size);
void deallocate_memory_ind (individual *ind);

double maximum (double a, double b);
double minimum (double a, double b);

void crossover (individual *parent1, individual *parent2, individual *child1, individual *child2);
void realcross (individual *parent1, individual *parent2, individual *child1, individual *child2);
void bincross (individual *parent1, individual *parent2, individual *child1, individual *child2);
void realcross2 (individual *parent1, individual *parent2, individual *child1, individual *child2);

void assign_crowding_distance_list (population *pop, list *lst, int front_size);
void assign_crowding_distance_indices (population *pop, int c1, int c2);
void assign_crowding_distance (population *pop, int *dist, int **obj_array, int front_size);

void decode_pop (population *pop);
void decode_ind (individual *ind);

void onthefly_display (population *pop, FILE *gp, int ii);

int check_dominance (individual *a, individual *b);

void evaluate_pop (population *pop);
void evaluate_ind (individual *ind);

void fill_nondominated_sort (population *mixed_pop, population *new_pop);
void crowding_fill (population *mixed_pop, population *new_pop, int count, int front_size, list *cur);

void initialize_pop (population *pop);
void initialize_ind (individual *ind);

void insert (list *node, int x);
list* del (list *node);

void merge(population *pop1, population *pop2, population *pop3);
void copy_ind (individual *ind1, individual *ind2);

void mutation_pop (population *pop);
void mutation_ind (individual *ind);
void bin_mutate_ind (individual *ind);
void bin_mutate_ind_mcda (individual *ind);
void bin_mutate_ind_mcda2 (individual *ind);
void real_mutate_ind (individual *ind);

//void test_problem (double *xreal, double *xbin, int **gene, double *obj, double *constr);
void test_problem (double *xreal, double *xbin, int **gene, double *obj, double *constr, sigma_crisp *S_crisp);

void assign_rank_and_crowding_distance (population *new_pop);

void report_pop (population *pop, FILE *fpt);
void report_feasible (population *pop, FILE *fpt);
void report_decode (population *pop, FILE *fpt);
void report_ind (individual *ind, FILE *fpt);

void quicksort_front_obj(population *pop, int objcount, int obj_array[], int obj_array_size);
void q_sort_front_obj(population *pop, int objcount, int obj_array[], int left, int right);
void quicksort_dist(population *pop, int *dist, int front_size);
void q_sort_dist(population *pop, int *dist, int left, int right);

void selection (population *old_pop, population *new_pop);
individual* tournament (individual *ind1, individual *ind2);


/* MCDA Methods */
extern double lmin;
extern double lmax;
extern double pmut_ind_bin;
extern int nalternatives;
double **Sigma;
double *lambdaset;
int **ordered_pairs;
extern int lambda_set_size;
sigma_crisp *S_crisp;

double calculate_lambda(double lowNum, double hiNum);
double get_lambda(double lowNum, double hiNum);
void calculate_preferenceMatrix(double l,int m, int Q[][m]);
void decode(int **gene, int m, int S[][m]);
void calculateRanking(int m, int R[], int S[][m]);
void calculateRanking2(double *xreal, int R[]);
int createPartitions(int P[], int m, double *xreal);
int getMaxPartition(int m, int P[]);
void deduceRelations(int P[], int m, int S[][m], int M, int L[][M]);
int calculateF(int m, int P[], int Q[][m], int M);
int calculateU(int m, int M, int L[][M], int P[], int Q[][m]);
void fillSigmaMatrix(char *fileName, char separator);
void discretizer();
void distance_matrix(int m, int Q[][m], double d[][m]);
void reencode(int m, int M, int K[], int L[][M], int S[][M], int **gene);
void fill_pais_structure();
void decode2(double *xreal, int m, int S[][m]);
void reencode2(int m, int M, int K[], int L[][M], int S[][M], double *xreal);
void initializePopulationDeterministic(individual *ind);
void mutation_pop2 (individual *ind);
int CalculatesR(int m, int Q[][m]);
double get_next_lambda(double l);
void k_means(int K, int m, int t, int L[], int V[m][2]);
void initialize_pop_k_means (int K, int parent[], int m, int Q[m][m], double d[m][m]);
void generate_mst (int mst[], int m, int Q[m][m], double d[m][m]);
void k_means2(int K, int m, int t, int L[], int Q[m][m], double d[m][m]);
void SortNN(int m, double d[], int sorted[]);
void mutation_pop3 (population *pop);
void nearest_neighbours(int m, int NN[m][m], double d[m][m]);
void get_partitions (double *xreal, int P[], double *obj);
int verifyOptimal(population *pop, int generations);
void mutate_ind (individual *ind);
void mutation_pop4 (population *pop);
void initialize_pop_random (population *pop);
void initialize_pop_mutation (population *pop);
void report_nondominated (population *pop, FILE *fpt);
void mutation_pop3_1 (population *pop);
void mutation_pop3_2 (population *pop);
void fill_crisp();
int calculate2F(int m, int P[], int M, double l);
int calculate2U(int m, int M, int L[][M], int P[], double l);
void deduceRelations2(int P[], int m, int M, int L[][M], sigma_crisp *S_crisp);
void distance_matrix2(int m, int Q[][m], double d[][m], double l);
//void deduceRelations3(int P[], int m, int M, int L[][M], double l)
void fill_dist();
sigma_crisp* get_s_crisp(double l);
void test_problem2 (double *xreal, double *obj, sigma_crisp *s_crisp, individual *ind);
int calculate3F(int m, int P[], int M, sigma_crisp *S_crisp);
int calculate3U(int m, int M, int L[][M], int P[], sigma_crisp *S_crisp);
void nearest_neighbours2(int m, int NN[m][m], sigma_crisp *S_crisp);
void fill_NN();
int Calculates2R(int m, sigma_crisp *S_crisp);
void deallocate_sigma();
void deallocate_crisp();
void deallocate_lambdaset();
double get_min_lambda();

# endif
