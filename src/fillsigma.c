#include <stdio.h>
#include <stdlib.h>

#include "global.h"

void fillSigmaMatrix(char *fileName, char separator){

FILE *sigmaFile;
char car, *cadena;
int pos, i,j;

cadena = (char *)malloc(15*sizeof(char));

if((sigmaFile=fopen(fileName,"r"))==NULL){
        printf("Unable to open the file .....%s", fileName);
        exit(1);
 }


 car = getc(sigmaFile);
 pos=0;

 while(car!='\n'){

     cadena[pos] = car;
     pos++;
     car = getc(sigmaFile);

 }

 nalternatives = atof(cadena);
 pos=0;

 Sigma = (double **)malloc(nalternatives*sizeof(double*));
  for (i=0;i<nalternatives;i++)
	  Sigma[i] = (double*)malloc(nalternatives*sizeof(double));


for(i=0; i < nalternatives; i++)
{
    for(j=0; j < nalternatives; j++)
    {
       car = getc(sigmaFile);
       while((car!=EOF) && (car!=separator) && (car!='\n')){
            cadena[pos] = car;
            pos++;
            car = getc(sigmaFile);

        }//end of while

       if(j == nalternatives - 1 && car==separator){
    	   car = getc(sigmaFile);
       }

       Sigma[i][j] = atof(cadena);
       pos=0;

    }//end of for j

}//end of for i

fclose(sigmaFile);


}//end of fillSigmaMatrix

void deallocate_sigma()
{
	int i;
	for (i=0;i<nalternatives;i++)
		free(Sigma[i]);

	free(Sigma);
}
